<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WeatherAPI\Model;

class Cities extends AbstractModel {

    public function getCities() {
        $this->getJson('https://raw.githubusercontent.com/David-Haim/CountriesToCitiesJSON/master/countriesToCities.json');   
        return $this->data;
    }    
}
