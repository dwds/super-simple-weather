<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WeatherAPI;

class CitiesController {
    
    public function getAllCities() {
        $all = new \WeatherAPI\Model\Cities();
        return $all->getCities();
    }
    
    public function getOnlyCities($data) {
        $onlycities = [];
        
        foreach ($data as $value) {
            foreach ($value as $city) {
                $onlycities[] = $city;
            }
        }
        
        return $onlycities;
    }
    
    public function getCitiesByCountry($data, $country) {
        $cities = [];
        
        foreach ($data as $key => $value) {
            if ($key === ucfirst($country)) {
                
                $cities = $value;                
            }
        }
        
        return $cities;       
        
    }
    
    public function getClosestMatch($word) {  
        $data = $this->getAllCities();
        $onlycities = $this->getOnlyCities($data);
       
        $bestmatch = "";
        $minDistance = PHP_INT_MAX;
        
        foreach ($onlycities as $value) {
          $distance = levenshtein($word, $value);
          if ($distance < $minDistance) {
            $minDistance = $distance;
            $bestmatch = $value;
          }
        }
             
        return $bestmatch;
    }
    
    public function getClosestMatchWithCountry($word, $country) {  
        $data = $this->getAllCities();
                
        $cities = $this->getCitiesByCountry($data, $country);
       
        $bestmatch = "";
        $minDistance = PHP_INT_MAX;
        
        foreach ($cities as $value) {
          $distance = levenshtein($word, $value);
          if ($distance < $minDistance) {
            $minDistance = $distance;
            $bestmatch = $value;
          }
        }
             
        return $bestmatch;
    }    
    
}