function getCityByAddress(cityAddress) {
    $.ajax({
        'url': 'http://maps.google.com/maps/api/geocode/json?address=' + cityAddress,
        'type': 'GET',
        'success': function (data) {
            console.log(data);
            var lng = data.results[0].geometry.location.lng;
            var lat = data.results[0].geometry.location.lat;
            console.log(lng);
            initMap(lat, lng);
        }
    });
}

var map;
var city = $('#map').data('city');

function initMap(lat, lng) {
    var latLng = {lat: lat, lng: lng};
    map = new google.maps.Map(document.getElementById('map'), {
        center: latLng,
        zoom: 8
    });
    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        title: city + 'ek'
    });
}


console.log(city);
getCityByAddress(city);